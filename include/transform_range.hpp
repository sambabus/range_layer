//

//          Copyright Sundeep S. Sangha 2015 - 2017.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef RANGE_LAYER_TRANSFORM_RANGE_HPP
#define RANGE_LAYER_TRANSFORM_RANGE_HPP

#include "range.hpp"
#include "bits/transform_range_fwd.hpp"
#include "bits/transform_range.hpp"
#include "bits/transform_range.tcc"
#include "bits/input_transform_range.hpp"
#include "bits/input_transform_range.tcc"
#include "bits/output_transform_range.hpp"
#include "bits/output_transform_range.tcc"

#endif

