//

//          Copyright Sundeep S. Sangha 2013 - 2017.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef RANGE_LAYER_GETOPT_HPP
#define RANGE_LAYER_GETOPT_HPP

#include "bits/getopt_fwd.hpp"
#include "bits/program_option.hpp"
#include "range_traits.hpp"
#include "bits/getopt_range.hpp"

#endif

