//

//          Copyright Sundeep S. Sangha 2015 - 2017.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef RANGE_LAYER_ALGO_READ_HPP
#define RANGE_LAYER_ALGO_READ_HPP

namespace range_layer {

template <typename Range, typename T, typename Pred>
Range
find (
  execution_policy::sequenced
, Range
, T const
, Pred
);

} /* range layer */
#endif

