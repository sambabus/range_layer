//

//          Copyright Sundeep S. Sangha 2015 - 2017.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef RANGE_LAYER_ARRAY_RANGE_HPP
#define RANGE_LAYER_ARRAY_RANGE_HPP

#include <cstddef>
#include "range_traits.hpp"
#include "bits/array_range_fwd.hpp"
#include "bits/array_range.hpp"
#include "bits/array_range.tcc"

#endif
