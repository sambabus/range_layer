//

//          Copyright Sundeep S. Sangha 2013 - 2017.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef RANGE_LAYER_GETOPT_CPP
#define RANGE_LAYER_GETOPT_CPP

#include "../include/getopt.hpp"
#include "bits/getopt_range.cpp"
#include "bits/program_option.cpp"

#endif

